#### https://hub.docker.com/_/node
FROM node:lts-alpine

ARG BUILD_DATE
LABEL org.label-schema.name="Unlighthouse" \
      org.label-schema.description="Unlighthouse Docker image based official node LTS image" \
      org.label-schema.vendor="epicsoft LLC / Alexander Schwarz <as@epicsoft.one>" \
      org.label-schema.url="https://gitlab.com/epicsoft-networks/unlighthouse/" \
      org.label-schema.vcs-url="https://gitlab.com/epicsoft-networks/unlighthouse/tree/main/" \
      org.label-schema.version="1.0" \
      org.label-schema.schema-version="1.0" \
      org.label-schema.build-date=${BUILD_DATE}

LABEL image.name="epicsoft_unlighthouse" \
      image.description="Unlighthouse Docker image based official node LTS image" \
      maintainer="epicsoft LLC" \
      maintainer.name="Alexander Schwarz <as@epicsoft.one>" \
      maintainer.url="https://epicsoft.one/" \
      maintainer.copyright="Copyright 2024 epicsoft LLC / Alexander Schwarz" \
      license="MIT"

USER root

RUN apk --no-cache add ca-certificates \
                       chromium \
 && update-ca-certificates \
 && rm -rf /var/cache/apk/*

RUN npm install -g unlighthouse

RUN npm install -g wrangler

RUN mkdir -p /unlighthouse/reports

COPY "unlighthouse.config.ts" "/root/unlighthouse.config.ts"

WORKDIR /root

CMD [ "unlighthouse", "--help" ]
