export default {
    outputPath: "/unlighthouse/reports",
    puppeteerOptions: {
        args: ["--no-sandbox"],
    }
}
